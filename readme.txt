Le projet est prévu pour fonctionner avec une base de données MYSQL.
Ci-dessous la configuration d'origine de la BDD, si vous désirez faire la votre il faut changer les valeurs datasources correspondante
dans le fichier application.properties:
- La Database doit s'appeler carlib.
- Le user doit s'appeller root.
- Le password doit être q3mERAeGaWDsomC8G.

Toute les autres variables devraient être laissés tel que fournis dans le repository. Elles contiennent l'accès au S3 repository pour
la gestion des images[les variables aws], le numéro de port pour le lancement du serveur et l'url par défaut d'enregistrement des images.

Le projet est un projet Java version 21 avec Spring Boot version 3.2.4. Maven est requis pour faire fonctionner le projet.

Le Swagger est consultable à l'url: http://localhost:3001/swagger-ui/index.html#/
Pour l'utiliser vous pouvez utiliser le /api/auth/login pour vous authentifier. Copier le token reçu et insérer le dans
authorize. Les routes protégés pourront alors directement être utilisés dans le swagger.

Les variables d'environnements doivent être complétés comme suit:
spring.application.name=Nom_du_projet
spring.datasource.url=url_connection_database
spring.datasource.username=usernamedatabase
spring.datasource.password=passworddatabase
spring.sql.init.mode=always
logging.level.org.springframework.security=TRACE
server.port=3001
aws.access.key=awsaccesskey
aws.secret.key=awssecretkey
aws.s3.bucket=nomdubucketaws
secret.key=random_key_pour_jwt
url.server.images=http://localhost:3001/api/images/rentals/
