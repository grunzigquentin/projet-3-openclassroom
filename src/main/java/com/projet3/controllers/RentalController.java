package com.projet3.controllers;

import com.amazonaws.services.s3.model.S3Object;
import com.projet3.model.entity.EntityRental;
import com.projet3.model.mapper.Rental.AllRentalMapper;
import com.projet3.model.mapper.Rental.RentalMapper;
import com.projet3.model.mapper.Rental.UpdateRentalMapper;
import com.projet3.model.request.RentalRequest;
import com.projet3.model.response.AllRentalDto;
import com.projet3.model.response.PutRentalResponse;
import com.projet3.model.response.RentalDto;
import com.projet3.model.response.CreateRentalResponse;
import com.projet3.services.RentalService;
import com.projet3.services.S3Service;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController()
@RequestMapping("/api")
public class RentalController {
    @Autowired
    RentalService rentalService;
    @Autowired
    S3Service s3Service;
    @PostMapping("/rentals")
    @Operation(security = { @SecurityRequirement(name = "Authorization") })
    public String createRental(@RequestParam("picture") MultipartFile file, RentalRequest rentalRequest, Principal principal) throws IOException {
        rentalService.createRental(rentalRequest, principal.getName(), file);
        return new CreateRentalResponse().toString();
    }

    @GetMapping("/rentals")
    @Operation(security = { @SecurityRequirement(name = "Authorization") })
    public AllRentalDto rentals(){
        List<EntityRental> entityRentals = rentalService.getAllRentals();
        List<RentalDto> listRentalDto= AllRentalMapper.INSTANCE.rentaltoRentalDto(entityRentals);
       return new AllRentalDto(listRentalDto);
    }
    @GetMapping("/rentals/{id}")
    @Operation(security = { @SecurityRequirement(name = "Authorization") })
    public RentalDto rental(@PathVariable Integer id){
        EntityRental entityRentals = rentalService.getRental(id);
        return RentalMapper.INSTANCE.rentaltoRentalDto(entityRentals);
    }

    @PutMapping("/rentals/{id}")
    @Operation(security = { @SecurityRequirement(name = "Authorization") })
    public String updateRental(@PathVariable Integer id, RentalRequest rentalRequest){
        EntityRental entityRental = UpdateRentalMapper.INSTANCE.rentalRequestToDbRental(rentalRequest);
        rentalService.updateRental(entityRental, id);
        return new PutRentalResponse().toString();
    }

    @GetMapping("/images/rentals/{fileName}")
    public ResponseEntity<InputStreamResource> viewFile(@PathVariable String fileName) {
        S3Object s3Object = s3Service.getFile(fileName);
        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_PNG)
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\""+fileName+"\"")
                .body(new InputStreamResource(s3Object.getObjectContent()));
        }
}
