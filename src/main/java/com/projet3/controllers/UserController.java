package com.projet3.controllers;

import com.projet3.model.entity.EntityUser;
import com.projet3.model.mapper.User.UserMapper;
import com.projet3.model.response.UserDto;
import com.projet3.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping("{id}")
    @Operation(security = { @SecurityRequirement(name = "Authorization") })
   public UserDto getUserById(@PathVariable Integer id){
        EntityUser entityUser = userService.getUserById(id);
        return UserMapper.INSTANCE.dbUserToUserDto(entityUser);
    }
}
