package com.projet3.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.projet3.model.entity.EntityUser;
import com.projet3.model.mapper.User.UserMapper;
import com.projet3.model.request.Logging;
import com.projet3.model.response.UserDto;
import com.projet3.model.request.AuthRequest;
import com.projet3.services.JWTService;
import com.projet3.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@RestController()
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    UserService userService;
    @Autowired
    private JWTService jwtService;

    @Autowired
  AuthenticationManager authenticationManager;

    @GetMapping("/me")
    @Operation(security = { @SecurityRequirement(name = "Authorization") })
    public UserDto getUser(Principal principal){
        EntityUser user = userService.getUser(principal.getName());
        return UserMapper.INSTANCE.dbUserToUserDto(user);
    }
    @PostMapping("/login")
    public String token(@RequestBody Logging logging) throws JsonProcessingException {
        return userService.userAuthentication(logging);
    }

    @PostMapping("/register")
    public String token(@RequestBody AuthRequest authRequest) throws JsonProcessingException {
        userService.saveUser(authRequest);
        Logging logging = new Logging(authRequest.email(), authRequest.password());
        return userService.userAuthentication(logging);
    }
}
