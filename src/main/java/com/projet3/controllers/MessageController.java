package com.projet3.controllers;

import com.projet3.model.request.MessageRequest;
import com.projet3.model.response.MessageResponse;
import com.projet3.services.MessageService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MessageController {
    @Autowired
    MessageService messageService;

    @PostMapping("/messages")
    @Operation(security = { @SecurityRequirement(name = "Authorization") })
    public String createMessage(@RequestBody MessageRequest messageRequest){
        messageService.saveEntityMessage(messageRequest);
        return new MessageResponse().toString();
    }
}
