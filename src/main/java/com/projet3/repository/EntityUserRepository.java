package com.projet3.repository;

import com.projet3.model.entity.EntityUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityUserRepository extends JpaRepository<EntityUser, Integer> {
    public EntityUser findByEmail(String email);

    public EntityUser getUserById(Integer id);
}