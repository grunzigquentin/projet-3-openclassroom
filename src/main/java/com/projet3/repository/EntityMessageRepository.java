package com.projet3.repository;

import com.projet3.model.entity.EntityMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityMessageRepository  extends JpaRepository<EntityMessage, Integer> {
}
