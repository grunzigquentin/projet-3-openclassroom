package com.projet3.repository;

import com.projet3.model.entity.EntityRental;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EntityRentalRepository extends JpaRepository<EntityRental, Integer> {
    public EntityRental getRentalById(Integer id);
}