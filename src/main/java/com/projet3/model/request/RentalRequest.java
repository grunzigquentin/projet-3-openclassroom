package com.projet3.model.request;

public record RentalRequest(String name,
                            double surface,
                            double price,
                            String description
                            ) {
}
