package com.projet3.model.request;

public record MessageRequest(String message,
                             Integer user_id,
                             Integer rental_id
) {
}