package com.projet3.model.request;

import java.util.Objects;

public record AuthRequest(
        String name,
        String password,
        String email) {
    public AuthRequest {
        Objects.requireNonNull(name, "name cannot be null");
        Objects.requireNonNull(password, "password cannot be null");
        Objects.requireNonNull(email, "email cannot be null");
    }
}

