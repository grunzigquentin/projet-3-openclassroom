package com.projet3.model.response;

public class TokenResponse {
    public String token;

    public TokenResponse(String jwt){
        this.token = jwt;
    }

    @Override
    public String toString() {
        return "{\"token\"" + ":" + "\""  + this.token + "\"}";
    }
}
