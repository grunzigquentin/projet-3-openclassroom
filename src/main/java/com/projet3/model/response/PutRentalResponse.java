package com.projet3.model.response;

public class PutRentalResponse {
    public String message = "Rental updated !";

    @Override
    public String toString() {
        return "{\"message\"" + ":" + "\""  + this.message + "\"}";
    }
}
