package com.projet3.model.response;

public class RentalDto {

    public String id;
    public String name;
    public String surface;
    public String price;
    public String picture;
    public String description;
    public String created_at;
    public String updated_at;

}

