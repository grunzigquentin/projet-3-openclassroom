package com.projet3.model.response;

public class CreateRentalResponse {
    public String message = "Rental created !";

    @Override
    public String toString() {
        return "{\"message\"" + ":" + "\""  + this.message + "\"}";
    }
}
