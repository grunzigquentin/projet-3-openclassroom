package com.projet3.model.response;

import java.util.List;

public class AllRentalDto {
    public List<RentalDto> rentals;

   public AllRentalDto(List<RentalDto> rentals) {
        this.rentals = rentals;
   }

}
