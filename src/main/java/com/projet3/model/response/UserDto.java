package com.projet3.model.response;

public class UserDto {
    private final Integer id;
    private final String name;
    private final String email;
    private final String created_at;
    private final String updated_at;
    public UserDto(Integer id, String name, String email, String created_at, String updated_at){
    this.name = name;
    this.email = email;
    this.id = id;
    this.created_at = created_at;
    this.updated_at = updated_at;
    }

    public String getEmail() {
        return email;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}