package com.projet3.model.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Set;

@Entity
@Table(name = "RENTALS")
@DynamicUpdate
public class EntityRental {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private double surface;
    private double price;
    @Column(updatable = false)
    private String picture;
    private String description;
    @ManyToOne()
    private EntityUser owner;
    @OneToMany(mappedBy = "rental")
    private Set<EntityMessage> message;

    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp created_at;
    @UpdateTimestamp
    private Timestamp updated_at;

    public EntityRental(String name, double surface, double
            price, String picture, String description, EntityUser owner){
        this.name = name;
        this.surface = surface;
        this.description = description;
        this.picture = picture;
        this.price = price;
        this.owner = owner;
    }

    public EntityRental(){}

    public String getUpdated_at() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY/MM/dd");
        return dateFormat.format(updated_at);
    }

    public String getCreated_at() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY/MM/dd");
        return dateFormat.format(created_at);
    }

    public EntityUser getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getSurface() {
        return surface;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setOwner(EntityUser owner) {
        this.owner = owner;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSurface(double surface) {
        this.surface = surface;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }
}