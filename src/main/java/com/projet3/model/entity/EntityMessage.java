package com.projet3.model.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "MESSAGES")
public class EntityMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String message;

    @ManyToOne()
    private EntityRental rental;

    @ManyToOne()
    private EntityUser user;


    public EntityMessage(String message, EntityRental entityRental, EntityUser entityUser){
        this.message = message;
        this.rental = entityRental;
        this.user = entityUser;
    }
}
