package com.projet3.model.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Set;

@Entity
@Table(name = "USERS")
public class EntityUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String password;
    private final String role = "USER";
    @Column(unique = true)
    private String email;
    private String name;

    @CreationTimestamp
    private Timestamp created_at;
    @UpdateTimestamp
    private Timestamp updated_at;

    @OneToMany(mappedBy = "owner")
    private Set<EntityRental> rentals;
    @OneToMany(mappedBy = "user")
    private Set<EntityMessage> message;

    public EntityUser(String name, String password, String email) {
        this.email = email;
        this.name = name;
        this.password = password;
    }
    public EntityUser() {
        this.name = null;
        this.password = null;
    }

    public EntityUser(String name, Integer id, Timestamp created_at, Timestamp updated_at, String email){
        this.name = name;
        this.id = id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.email = email;

    }

    @Override
    public String toString(){
        return this.name;
    }
    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public Integer getId() {
        return id;
    }
    public String getEmail() {
        return email;
    }

    public String getUpdated_at() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY/MM/dd");
        return dateFormat.format(updated_at);
    }

    public String getCreated_at() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY/MM/dd");
        return dateFormat.format(created_at);
    }

}