package com.projet3.model.mapper.Rental;

import com.projet3.model.entity.EntityRental;
import com.projet3.model.response.RentalDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RentalMapper {
    RentalMapper INSTANCE = Mappers.getMapper( RentalMapper.class);

    RentalDto rentaltoRentalDto(EntityRental entityRentals);
}
