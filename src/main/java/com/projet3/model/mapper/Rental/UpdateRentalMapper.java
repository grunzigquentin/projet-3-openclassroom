package com.projet3.model.mapper.Rental;

import com.projet3.model.entity.EntityRental;
import com.projet3.model.request.RentalRequest;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UpdateRentalMapper {
    UpdateRentalMapper INSTANCE = Mappers.getMapper( UpdateRentalMapper.class);


    EntityRental rentalRequestToDbRental(RentalRequest rentalRequest);
}
