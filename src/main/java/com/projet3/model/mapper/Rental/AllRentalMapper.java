package com.projet3.model.mapper.Rental;

import com.projet3.model.entity.EntityRental;
import com.projet3.model.response.RentalDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AllRentalMapper {
    AllRentalMapper INSTANCE = Mappers.getMapper( AllRentalMapper.class);

    List<RentalDto> rentaltoRentalDto(List<EntityRental> entityRentals);
}
