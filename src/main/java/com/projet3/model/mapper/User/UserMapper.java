package com.projet3.model.mapper.User;

import com.projet3.model.entity.EntityUser;
import com.projet3.model.response.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class);


    UserDto dbUserToUserDto(EntityUser entityUser);
}
