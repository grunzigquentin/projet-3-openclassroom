package com.projet3.configuration;

import com.projet3.model.entity.EntityUser;
import com.projet3.repository.EntityUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service

public class CustomerUserDetailsService implements UserDetailsService {

    @Autowired
    private EntityUserRepository entityUserRepository;
    @Override
    public CustomerUserDetails loadUserByUsername(String email) throws UsernameNotFoundException{
        EntityUser user = entityUserRepository.findByEmail(email);
        return new CustomerUserDetails(user.getEmail(), user.getPassword(), user.getId(), getGrantedAuthorities(user.getRole()));
    }

    private List<GrantedAuthority> getGrantedAuthorities(String role) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
        return authorities;
    }
}
