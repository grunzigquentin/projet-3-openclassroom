package com.projet3.services;

import com.projet3.model.entity.EntityRental;
import com.projet3.model.entity.EntityUser;
import com.projet3.model.entity.EntityMessage;
import com.projet3.model.request.MessageRequest;
import com.projet3.repository.EntityMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
    @Autowired
    public EntityMessageRepository entityMessageRepository;

    @Autowired UserService userService;
    @Autowired RentalService rentalService;

    public void saveEntityMessage(MessageRequest messageRequest){
        EntityUser entityUser = userService.getUserById(messageRequest.user_id());
        EntityRental entityRental =rentalService.getRental(messageRequest.rental_id());
        EntityMessage entityMessage = new EntityMessage(messageRequest.message(), entityRental, entityUser);
        entityMessageRepository.save(entityMessage);
    }
}
