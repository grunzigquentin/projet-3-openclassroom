package com.projet3.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.projet3.model.entity.EntityUser;
import com.projet3.model.request.AuthRequest;
import com.projet3.model.request.Logging;
import com.projet3.model.response.TokenResponse;
import com.projet3.repository.EntityUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private EntityUserRepository entityUserRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    private JWTService jwtService;

    public EntityUser saveUser(AuthRequest user){

        EntityUser newUser = new EntityUser(user.name(), bCryptPasswordEncoder.encode(user.password()), user.email());

        return entityUserRepository.save(newUser);
    }

    public EntityUser getUser(String email){
        return entityUserRepository.findByEmail(email);
    }

    public EntityUser getUserById(Integer id){
        return entityUserRepository.getUserById(id);
    }

    /**
     * @param logging is the object containing the email and password used for authentication
     * @return TokenResponse in a json format
     */
    public String userAuthentication(Logging logging) throws JsonProcessingException {
        UsernamePasswordAuthenticationToken log = UsernamePasswordAuthenticationToken.unauthenticated(logging.email(), logging.password());
        Authentication authentication = authenticationManager.authenticate(log);
        TokenResponse tokenResponse = new TokenResponse(jwtService.generateToken(authentication));

        return tokenResponse.toString();
    }
}
