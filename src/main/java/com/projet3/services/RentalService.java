package com.projet3.services;

import com.projet3.model.entity.EntityRental;
import com.projet3.model.entity.EntityUser;
import com.projet3.model.request.RentalRequest;
import com.projet3.repository.EntityRentalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class RentalService {
    @Autowired
    public EntityRentalRepository entityRentalRepository;
    @Autowired
    public UserService userService;
    @Autowired
    public S3Service s3Service;
    @Value("${url.server.images}")
    public String urlImages;
    public EntityRental createRental(RentalRequest rentalRequest, String name, MultipartFile file) throws IOException {
        String fileUrl = this.saveFile(file);
        EntityUser user = userService.getUser(name);
        EntityRental entityRental = new EntityRental(rentalRequest.name(), rentalRequest.surface(), rentalRequest.price(), fileUrl, rentalRequest.description(), user);
        return entityRentalRepository.save(entityRental);
    }

    public String saveFile(MultipartFile file) throws IOException {
        s3Service.uploadFile(file.getOriginalFilename(), file);
        return urlImages + file.getOriginalFilename();
    }
    public List<EntityRental>getAllRentals(){
        return entityRentalRepository.findAll();
    }

    public EntityRental getRental(Integer id){
        return entityRentalRepository.getRentalById(id);
    }

    public void updateRental(EntityRental entityRental, Integer id){
       EntityRental oldRental = entityRentalRepository.getRentalById(id);
       entityRental.setOwner(oldRental.getOwner());
       entityRental.setId(id);
        entityRentalRepository.save(entityRental);
    }
}